# revlines 
    revlines [length] [<infile] [>outfile]

revlines reverses the order of lines of text, optionally grouped by a length.

i.e. given:
a
b
c
d

revlines => d, c, b, a
revlines 2 => c, d, a, b
