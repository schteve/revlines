/* revlines.cc
 */

#include "revlinesFunc.h"

int main(int argc, char **argv)
{
    unsigned length = 1;
    
    if (argc > 1) {
        if (isdigit(argv[1][0])) {
            length = std::stoi(argv[1]);
        } else {
            Usage();
            return 0;
        }
    }

    Reverse(length);

    return 0;
}
