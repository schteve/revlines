/* Externally defined functions for revlinesFunc.h
 */
#include "revlinesFunc.h"


void Usage()
{
    const std::string s =  "revlines Copyright © 2017-2018 Steven Mosele\n\
This program comes with ABSOLUTELY NO WARRANTY\n\
This is free software, and you are welcome to\n\
redistribute it under certain conditions; see\n\
GPL-3.0 at <http://www.gnu.org/licenses/gpl-3.0.html>\n\
\n\
revlines Usage\n\
revlines [length] [<infile] [>outfile]\n\
";
    std::cout << s << std::endl;
}

void Reverse(unsigned &length) 
{
    std::deque<svec> block;

    while (std::cin) {
        std::string line;
        svec group;
        unsigned cnt = 0;

        while (cnt < length && getline(std::cin, line)) {
            group.push_back(line);
            ++cnt;
        }
        block.push_front(group);
    }

    for (svec sv : block) {
        for (std::string s : sv) {
            std::cout << s << std::endl;
        }
    } 
}
