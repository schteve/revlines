/* revlinesFunc.h 
 */

#include <iostream>
#include <cctype>
#include <deque>
#include <string>
#include <vector>

typedef std::vector<std::string> svec;

void Usage();
void Reverse(unsigned&);
