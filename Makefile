# MAKEFILE FOR simple C++ programming
# orginally by David MacKay

# if you want the debugger kdbg to work nicely, REMOVE the -O2 flag
# if you want to get all warnings enabled, INCLUDE the -O2 flag

CFLAGS = $(INCDIRS)  \
	-pedantic -g -O2\
	-Wall -Wconversion\
	-Wformat  -Wshadow\
	-Wpointer-arith -Wcast-qual -Wwrite-strings\
	-D__USE_FIXED_PROTOTYPES__

LIBS = -l stdc++ -lm

CXX = g++

revlinesobjects = bin/revlines.o bin/revlinesFunc.o

revlines: $(revlinesobjects)
	$(CXX) $(CFLAGS) $(LIBS) -o bin/revlines $(revlinesobjects)

bin/revlines.o: src/revlines.cc src/revlinesFunc.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/revlines.cc -o bin/revlines.o

bin/revlinesFunc.o: src/revlinesFunc.cc src/revlinesFunc.h 
	$(CXX) $(CFLAGS) $(LIBS) -c src/revlinesFunc.cc -o bin/revlinesFunc.o

# Clean
clean: 
	rm bin/*.o
